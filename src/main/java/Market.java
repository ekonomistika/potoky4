import java.util.ArrayList;

public class Market {

    public  ArrayList<Stock> stock = new ArrayList<Stock>();

    public Market() {
        this.stock.add(new Stock("AAPL", 100, 141));
        this.stock.add(new Stock("COKE", 1000, 387));
        this.stock.add(new Stock("IBM", 200, 137));
    }

    public Market(ArrayList<Stock> stock) {
        this.stock = stock;
    }

    public ArrayList<Stock> getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
       // this.stock = stock;
    }
}
