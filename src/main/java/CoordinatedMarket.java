import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

public class CoordinatedMarket {

    public static void main(String[] args) {

        Trader traderAlice      = new Trader("Alice");
        Trader traderBob        = new Trader("Bob");
        Trader traderCharlie    = new Trader("Charlie");

        traderAlice.setStock(new Stock("AAPL", 10, 100));
        traderAlice.setStock(new Stock("COKE",20, 390));

        traderBob.setStock(new Stock("AAPL", 10, 140));
        traderBob.setStock(new Stock("IBM", 20, 135));

        traderCharlie.setStock(new Stock("COKE", 300, 370));

        Trader[] traders = {traderAlice, traderBob, traderCharlie};
        Stock[] marketStocs = {new Stock("AAPL", 100, 141), new Stock("COKE", 1000, 387), new Stock("IBM", 200, 137)};

        Market market = new Market(new ArrayList<>(Arrays.asList(marketStocs)));

        BrokerThread changePrice = new BrokerThread(new ArrayList<>(Arrays.asList(traders)), market);

        changePrice.start();

        ChangePricesThread changePricesThread = new ChangePricesThread(market);

        changePricesThread.start();

        System.out.println(LocalTime.now() + ": Початок виконання");
        
        for (int i = 0; i < 3; i++) {
            
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        }
    }









